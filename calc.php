<?php 
if (isset($_GET['submit'])) {
  $firstNum = $_GET['num1'];
  $secondNum = $_GET['num2'];
  $operator = $_GET['operator'];
    
  switch ($operator) {
    case "Add":
      echo $firstNum + $secondNum;
      break;
    case "Subtract":
      echo $firstNum - $secondNum;
      break;
    case "Multiply":
      echo $firstNum * $secondNum;
      break;
    case "Divide":
      if($secondNum==0){
        echo "Division with 0 is not possible, Enter some other number.";
      }
      else{
        echo $firstNum / $secondNum;
      }
      break;
    default:
      echo "You should to select an operator!";
      break;
}
}